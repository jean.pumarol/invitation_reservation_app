import React from 'react';
import PropTypes from 'prop-types';
import GuestInputForm from './GuestInputForm';

const Header = props =>
  <header>
    <h1>RSVP</h1>
    <p>A Treehouse App</p>

    <GuestInputForm
      pendingGuest={props.pendingGuest}
      toggleGuest={props.toggleGuest}
      newGuestSubmitForm={props.newGuestSubmitForm} />

  </header>

Header.propTypes = {
  pendingGuest: PropTypes.string.isRequired,
  toggleGuest: PropTypes.func.isRequired,
  newGuestSubmitForm: PropTypes.func.isRequired,
}

export default Header;
