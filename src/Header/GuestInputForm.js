import React from 'react';
import PropTypes from 'prop-types';

const GuestInputForm = props =>
  <form onSubmit={props.newGuestSubmitForm}>
      <input
        type="text"
        value={props.pendingGuest}
        placeholder="Invite Someone"
        onChange={props.toggleGuest}/>
      <button type="submit" name="submit" value="submit">Submit</button>
  </form>


GuestInputForm.propTypes = {
  pendingGuest: PropTypes.string.isRequired,
  toggleGuest: PropTypes.func.isRequired,
  newGuestSubmitForm: PropTypes.func.isRequired,
}

export default GuestInputForm;
